import { useContext } from 'react'
import { Link, useHistory } from "react-router-dom"
import { Navbar, Container, Nav } from 'react-bootstrap'
import UserContext from "../UserContext"

export default function AppNavbar(){
 const { user, unsetUser } = useContext(UserContext)
 const history = useHistory()

  const logout = () => {
    unsetUser()
    history.push("/login")
  }

   const myCart = () => {
    history.push("/myCart")
  }


   const rightNav = (!user.id) ? (
    <>
      <Link className="nav-link" to="/register">Register</Link>
      <Link className="nav-link" to="/login">Log In</Link>
    </>
  ) : (
  <>
    <Nav.Link onClick={myCart}>My Cart</Nav.Link>
    <Nav.Link onClick={logout}>Log Out</Nav.Link>
  </>
  )
 
  return(
 
    <Navbar bg="dark" variant="dark" expand="lg">
       <Container>
       <Link className="navbar-brand" to="/">Thrift En' Shop</Link>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Link className="nav-link" to="/products">Products</Link>
            {rightNav}
          </Nav>
        </Navbar.Collapse>
    </Container>
    </Navbar>

  )
}
