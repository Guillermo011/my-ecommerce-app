import { Link } from 'react-router-dom'
import { Card, Container } from 'react-bootstrap'


export default function CourseCard({productProp}){

	const { _id, productName, description, price } = productProp


	return(
		<div  className="col-md-6 col-12 my-2">
				<Card className="cardHighlight mx-3 text-center">
				<Card.Body>
					<Link to={`/products/${_id}`}> <Card.Title>{productName}</Card.Title> </Link>

					<Card.Text>Description: {description}</Card.Text>
					<Card.Subtitle>Price: {price}</Card.Subtitle>					
				
					<div className="card-footer">
			        <Link className="btn btn-primary btn-block" to={`/products/${_id}`}>Details</Link>
			        </div>
				</Card.Body>
				</Card>
		</div>


		)
}