import { useState,useEffect,useContext } from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory, Link } from 'react-router-dom'
import UserContext from '../UserContext'

export default function Login(){

	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	const history = useHistory()

	const loginUser = (e) => {
		e.preventDefault()
			fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => res.json())
			.then(data => {
				// console.log(data)

				if(typeof data.accessToken !== "undefined"){

					localStorage.setItem("token", data.accessToken)

					fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
						headers: {
							Authorization: `Bearer ${data.accessToken}`
						}
					})
					.then(res => res.json())
					.then(data => {
						// console.log(data)
						setUser({
							id: data._id,
							isAdmin: data.isAdmin
						})

							Swal.fire({
							title: "Login Successful",
							icon: "success",
							text: "Welcome to our Shop"
						})

						history.push("/")
					})


				}else{
							Swal.fire({
							title: "Authentication failed",
							icon: "error",
							text: "Check your login details and try again"
						})
				}

			})
	}


		useEffect(() => {
			if(email !== '' && password !== ''){
					setIsActive(true)
			}else{
					setIsActive(false)
			}

		}, [email, password])

	if (user.id !== null){
	Swal.fire({
	title: "Error",
	icon: "error",
	text: "You are already logged in"
	})	
		return <Redirect to="/"/>	
	}

	return(
		<div id="loginDiv" className="container mt-5 col-lg-6 ">
		<Form onSubmit={e => loginUser(e)} className="" id="loginForm">
			<h3 className="text-center my-3">Login</h3>
			<Form.Group controlId="email" className="pb-1 mx-5">
				<Form.Label id="loginLabel">User Email</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password" className="my-2 mx-5">
				<Form.Label id="loginLabel">Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>				
			</Form.Group>
<div className="text-center container">
			{isActive ? <Button variant="primary" type="submit" id="submitBtn" className="btn-block mt-3">Log In</Button> : <Button variant="secondary" id="submitBtn" className="btn-block mt-3" disabled>Log In</Button>}
</div>
		</Form>
			<h6 className="text-center my-3">Don't have an account yet? Click <Link to="/register">here</Link> to register.</h6>
		</div>
		)


}