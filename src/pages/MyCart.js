import { useState, useEffect, useContext } from 'react';
import { Link, useParams, useHistory } from 'react-router-dom';
import { Card, Button, Container, InputGroup, FormControl, Form, Table } from 'react-bootstrap';
import { add, total, list, get, exists, remove, quantity } from 'cart-localstorage'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function MyCart(){
	const [item, setItem] = useState([])
	const [table, setTable] = useState([])
	const [qty, setQty] = useState("")	



	useEffect(()=> {
	 let cartItems = list()


	 	console.log(cartItems)
		const listItems = cartItems.map((item) => {
			return (

		      <tr key={item.id}>
		          <td><Link to={`/products/${item.id}`}>{item.name}</Link></td>
		          <td>₱{item.price}</td>

		          <td>
					<button type="button" class="btn btn-sm btn-outline-primary"
								onClick={quantity(item.id,-1)}>-</button>
					{item.quantity}

    					<button type="button" class="btn btn-sm btn-outline-primary"
								onClick={quantity(item.id,1)}>+</button>
	          
		          </td>
		          <td>₱{item.price*item.quantity}</td>
		          <td className="text-center">
		          	<Button variant="danger" onClick={() => {remove(item.id)}}>Remove</Button>
		          </td>
		      </tr>			
			)
		})

				setTable(listItems)
	}, [])



	return(
		<Container>
			<div className="my-4 text-center">
			<h2>Shopping Cart</h2>
			</div>
		<Table striped bordered hover responsive="lg">
			<thead className="bg-dark text-white">
				<tr>
					<th>Name</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>

				{table}
			{table.length !== 0 ? <>
				<td colspan="3" className="text-center">Total Amount</td>
				<td className="text-center"><strong>₱{total()}</strong></td></> :
				<h2 className="text-center">Please add item to your shopping Cart</h2>}

			</tbody>
		</Table>
		</Container>
		)
}